# Evermos Test #

Build an App contains randomized image and descriptions from Pexels Api  in Flutter. 

### Supported devices ###

* Android devices
* UI Tested on OPPO A5s

### Supported features ###

* User can choose style of the list by tapping icon in the appbar (available options: grid view, list view)
* When user scrolls down app will load more item
* Each item should be tappable and directs user to detail page. The detail page must contain the photo itself in a larger size and its description (photographer name and photo URL) in a clear way. 

### Requirements to build the app ###

* visual Studio Code or Android Stuido
* Dart & Flutter
* Pexels Api : https://www.pexels.com/api/?locale=en-US

### Instructions to build and deploy the app ###

* to Build added these pub files in pubspcae.yaml :
* cached_network_image: ^3.1.0
* Pexels Api : https://www.pexels.com/api/?locale=en-US
* connectivity: ^3.0.6
* cupertino_icons: ^1.0.2
* equatable: ^2.0.3  
* flutter_bloc: ^7.1.0
* get: ^4.3.8
* http: ^0.13.3
* to Deploy : flutter clean & flutter build apk
