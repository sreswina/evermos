part of 'model.dart';

class PhotoModel {
  final Map<String, dynamic> map;

  PhotoModel(this.map);

  Photo toMovie() => Photo(
      id: map['id'],
      url: map['url'],
      photographer: map['photographer'],
      photographerurl: map['photographerurl'],
      photographerid: map['photographerid'],
      src: SrcPhoto.fromMap(map["src"]));
}
