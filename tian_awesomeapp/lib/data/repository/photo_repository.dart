part of 'repository.dart';

class PhotoRepository extends IPhotoRepository {
  final IPhotoServices photoServices;

  PhotoRepository(this.photoServices);

  @override
  FutureOr<List<Photo>> getPhotos(int page) async {
    return await photoServices.getPhotos(page);
  }
}
