import 'dart:async';

import 'package:tian_awesomeapp/data/services/services.dart';
import 'package:tian_awesomeapp/domain/entities/entities.dart';
import 'package:tian_awesomeapp/domain/repository/repository_interface.dart';

part 'photo_repository.dart';
