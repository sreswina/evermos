part of 'services.dart';

class PhotoServices implements IPhotoServices {
  @override
  Future<List<Photo>> getPhotos(int page, {http.Client? client}) async {
    String url =
        "https://api.pexels.com/v1/curated?api_key=$apiKey&per_page=10&page=$page";

    client ??= http.Client();

    var response = await client.get(Uri.parse(url));

    if (response.statusCode == 422) {
      return [];
    }

    var data = json.decode(response.body);

    List<dynamic> results = data['photos'];

    return results.map((result) => PhotoModel(result).toMovie()).toList();
  }
}
