import 'dart:async';
import 'dart:convert';

import 'package:tian_awesomeapp/data/model/model.dart';
import 'package:tian_awesomeapp/domain/entities/entities.dart';
import 'package:tian_awesomeapp/shared/shared_value.dart';
import 'package:http/http.dart' as http;

part 'photo_services.dart';

abstract class IPhotoServices {
  FutureOr<List<Photo>> getPhotos(int page, {http.Client? client});
}
