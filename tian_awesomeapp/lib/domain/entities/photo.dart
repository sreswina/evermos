part of 'entities.dart';

class Photo extends Equatable {
  final int id;
  final String url;
  final String? photographer;
  final String? photographerurl;
  final int? photographerid;
  final SrcPhoto src;

  const Photo({
    required this.id,
    required this.url,
    @required this.photographer,
    @required this.photographerurl,
    @required this.photographerid,
    required this.src,
  });

  @override
  List<Object?> get props =>
      [id, url, photographer, photographerurl, photographerid, src];
}
