class SrcPhoto {
  final String portrait;
  final String large;
  final String large2x;
  final String landscape;
  final String medium;
  final String small;

  SrcPhoto(
      {required this.portrait,
      required this.large,
      required this.large2x,
      required this.landscape,
      required this.medium,
      required this.small});

  factory SrcPhoto.fromMap(Map<String, dynamic> srcJson) {
    return SrcPhoto(
        portrait: srcJson["portrait"],
        large: srcJson["large"],
        large2x: srcJson["large2x"],
        landscape: srcJson["landscape"],
        medium: srcJson["medium"],
        small: srcJson["small"]);
  }
}
