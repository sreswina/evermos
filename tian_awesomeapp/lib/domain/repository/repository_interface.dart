import 'dart:async';

import 'package:tian_awesomeapp/domain/entities/entities.dart';

abstract class IPhotoRepository {
  FutureOr<List<Photo>> getPhotos(int page);
}
