part of 'usecase.dart';

class GetPhotosParameter {
  final int page;

  GetPhotosParameter(this.page);
}

class GetPhotosUsecase implements IUsecase<List<Photo>, GetPhotosParameter> {
  final IPhotoRepository photoRepository;

  GetPhotosUsecase(this.photoRepository);

  @override
  FutureOr<List<Photo>> execute(GetPhotosParameter parameter) async {
    return await photoRepository.getPhotos(parameter.page);
  }
}
