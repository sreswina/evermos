import 'dart:async';

import 'package:tian_awesomeapp/domain/entities/entities.dart';
import 'package:tian_awesomeapp/domain/repository/repository_interface.dart';

part 'get_photos_usecase.dart';

abstract class IUsecase<T, P> {
  FutureOr<T> execute(P parameter);
}

class None {}
