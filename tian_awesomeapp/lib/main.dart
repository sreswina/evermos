import 'package:tian_awesomeapp/presentation/bloc/network_cek_bloc.dart';
import 'package:tian_awesomeapp/presentation/bloc/photo_bloc.dart';
import 'package:tian_awesomeapp/presentation/page/page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (_) => NetworkCekBloc()
              ..add(
                ListenConnection(),
              )),
        BlocProvider(
            create: (_) => PhotoBloc()
              ..add(
                FetchPhotos(1),
              )),
      ],
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
        home: MainPage(),
      ),
    );
  }
}
