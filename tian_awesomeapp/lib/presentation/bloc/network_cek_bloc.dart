import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';

part 'network_cek_event.dart';
part 'network_cek_state.dart';

class NetworkCekBloc extends Bloc<NetworkCekEvent, NetworkCekState> {
  NetworkCekBloc() : super(ConnectionInitial());

  StreamSubscription? _subscription;

  @override
  Stream<NetworkCekState> mapEventToState(
    NetworkCekEvent event,
  ) async* {
    if (event is ListenConnection) {
      _subscription = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        add(ConnectionChanged(result == ConnectivityResult.none
            ? ConnectionFailure()
            : result == ConnectivityResult.mobile
                ? ConnectionSuccess()
                : ConnectionSuccess()));
      });
    }
    if (event is ConnectionChanged) yield event.connection;
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }
}
