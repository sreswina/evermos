part of 'network_cek_bloc.dart';

abstract class NetworkCekEvent {}

class ListenConnection extends NetworkCekEvent {}

class ConnectionChanged extends NetworkCekEvent {
  NetworkCekState connection;
  ConnectionChanged(this.connection);
}
