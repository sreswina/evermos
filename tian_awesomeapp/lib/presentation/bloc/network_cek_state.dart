part of 'network_cek_bloc.dart';

abstract class NetworkCekState {}

class ConnectionInitial extends NetworkCekState {}

class ConnectionSuccess extends NetworkCekState {}

class ConnectionFailure extends NetworkCekState {}
