import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:tian_awesomeapp/data/repository/repository.dart';
import 'package:tian_awesomeapp/data/services/services.dart';
import 'package:tian_awesomeapp/domain/entities/entities.dart';
import 'package:tian_awesomeapp/domain/usecase/usecase.dart';
import 'package:equatable/equatable.dart';

part 'photo_event.dart';
part 'photo_state.dart';

class PhotoBloc extends Bloc<PhotoEvent, PhotoState> {
  PhotoBloc() : super(PhotoUninitialized());

  @override
  Stream<PhotoState> mapEventToState(
    PhotoEvent event,
  ) async* {
    List<Photo> photos;
    if (state is PhotoUninitialized) {
      photos = await GetPhotosUsecase(PhotoRepository(PhotoServices()))
          .execute(GetPhotosParameter(1));

      yield PhotoLoaded(photos: photos, hasReachedMax: false);
      return;
    } else {
      PhotoLoaded photoLoaded = state as PhotoLoaded;
      photos = await GetPhotosUsecase(PhotoRepository(PhotoServices()))
          .execute(GetPhotosParameter(photoLoaded.photos.length));

      yield (photos.isEmpty)
          ? photoLoaded.copyWith(
              photos: photoLoaded.photos, hasReachedMax: true)
          : PhotoLoaded(
              photos: photoLoaded.photos + photos, hasReachedMax: false);
    }
  }
}
