part of 'photo_bloc.dart';

abstract class PhotoEvent extends Equatable {
  const PhotoEvent();

  @override
  List<Object> get props => [];
}

class FetchPhotos extends PhotoEvent {
  final int page;

  FetchPhotos(this.page);

  @override
  List<Object> get props => [page];
}
