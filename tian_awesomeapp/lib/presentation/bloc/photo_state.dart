part of 'photo_bloc.dart';

abstract class PhotoState extends Equatable {
  const PhotoState();

  @override
  List<Object> get props => [];
}

class PhotoUninitialized extends PhotoState {}

class PhotoLoaded extends PhotoState {
  final List<Photo> photos;
  final bool hasReachedMax;

  PhotoLoaded({
    required this.photos,
    required this.hasReachedMax,
  });

  PhotoLoaded copyWith({
    required List<Photo> photos,
    required bool hasReachedMax,
  }) {
    return PhotoLoaded(
      hasReachedMax: hasReachedMax,
      photos: photos,
    );
  }

  @override
  List<Object> get props => [photos, hasReachedMax];

  @override
  String toString() =>
      "{ PhotoLoaded : { movies: ${photos.length}, hasReachedMax: $hasReachedMax } }";
}
