part of 'page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

enum LayoutStatus { lyGrid, lyList }

class _MainPageState extends State<MainPage> {
  LayoutStatus _layoutSatatus = LayoutStatus.lyList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<NetworkCekBloc, NetworkCekState>(
        builder: (context, state) {
          if (state is ConnectionFailure)
            return Center(
                child: Text("No Internet Connection",
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )));
          if (state is ConnectionSuccess)
            return NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverOverlapAbsorber(
                    handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                        context),
                    sliver: SliverAppBar(
                      backgroundColor: Colors.purple,
                      title: Text('Awesome App'),
                      expandedHeight:
                          200.0, // This is the title in the app bar.
                      pinned: true,
                      forceElevated: innerBoxIsScrolled,
                      flexibleSpace: FlexibleSpaceBar(
                        background: Hero(
                            tag: 11,
                            child: CachedNetworkImage(
                                imageUrl: 'https://picsum.photos/250?image=17',
                                placeholder: (context, url) => Container(
                                      color: Color(0xfff5f8fd),
                                    ),
                                fit: BoxFit.cover)),
                      ),
                      actions: [
                        IconButton(
                            icon: Icon(Icons.grid_view),
                            onPressed: () {
                              _buildLayout(1);
                            }),
                        IconButton(
                          icon: Icon(Icons.list_alt),
                          onPressed: () {
                            _buildLayout(2);
                          },
                        ),
                      ],
                    ),
                  ),
                ];
              },
              body: SafeArea(
                top: false,
                bottom: false,
                child: _buildShow(),
              ),
            );
          else
            return Text("");
        },
      ),
    );
  }

  _buildLayout(var a) {
    setState(() {
      _layoutSatatus = a == 1 ? LayoutStatus.lyGrid : LayoutStatus.lyList;
    });
  }

  _buildShow() {
    switch (_layoutSatatus) {
      case LayoutStatus.lyList:
        return PhotoPage();
      case LayoutStatus.lyGrid:
        return PhotoGrid();
    }
  }
}
