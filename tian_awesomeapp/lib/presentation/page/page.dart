import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:tian_awesomeapp/domain/entities/entities.dart';
import 'package:tian_awesomeapp/presentation/bloc/network_cek_bloc.dart';
import 'package:tian_awesomeapp/presentation/bloc/photo_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'main_page.dart';
part 'photo_page.dart';
part 'photo_page_grid.dart';
part 'photo_detail.dart';
