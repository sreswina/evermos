part of 'page.dart';

class PhotoDetail extends StatelessWidget {
  final Photo photo;

  PhotoDetail(this.photo);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.purple,
                title: Text('Awesome App'),
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  background: Hero(
                    tag: photo.id,
                    child: CachedNetworkImage(
                        imageUrl: photo.src.large2x,
                        placeholder: (context, url) => Container(
                              color: Color(0xfff5f8fd),
                            ),
                        fit: BoxFit.cover),
                  ),
                ),
              )
            ];
          },
          body: Stack(
            children: <Widget>[
              Positioned(
                top: 30.0,
                left: 10.0,
                right: 10.0,
                child: Column(
                  children: <Widget>[
                    Text(photo.photographer ?? photo.photographer!),
                    Text(photo.url),
                    Divider(
                      color: Colors.black45,
                    ),
                  ],
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
