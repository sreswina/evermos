part of 'page.dart';

class PhotoPage extends StatefulWidget {
  const PhotoPage({Key? key}) : super(key: key);

  @override
  _PhotoPageState createState() => _PhotoPageState();
}

class _PhotoPageState extends State<PhotoPage> {
  final _key = GlobalKey<FormState>();
  ScrollController controller = ScrollController();
  late PhotoBloc bloc;

  void onScroll() {
    double maxScroll = controller.position.maxScrollExtent;
    double currenSacroll = controller.position.pixels;

    if (currenSacroll == maxScroll) bloc.add(FetchPhotos(1));
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<PhotoBloc>(context);
    controller.addListener(onScroll);
    return BlocBuilder<PhotoBloc, PhotoState>(
      builder: (_, photoState) => photoState is PhotoLoaded
          ? Builder(
              builder: (BuildContext context) {
                return CustomScrollView(
                  controller: controller,
                  key: _key,
                  slivers: <Widget>[
                    SliverOverlapInjector(
                      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                          context),
                    ),
                    SliverPadding(
                      padding: const EdgeInsets.all(8.0),
                      sliver: SliverFixedExtentList(
                        itemExtent: 48.0,
                        delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            return (index < photoState.photos.length)
                                ? Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: InkWell(
                                      onTap: () {
                                        Get.to(PhotoDetail(
                                            photoState.photos[index]));
                                      },
                                      child: ListTile(
                                        leading: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            child: kIsWeb
                                                ? Image.network(
                                                    photoState.photos[index].src
                                                        .medium,
                                                    height: 50,
                                                    width: 80,
                                                    fit: BoxFit.cover,
                                                  )
                                                : CachedNetworkImage(
                                                    imageUrl: photoState
                                                        .photos[index]
                                                        .src
                                                        .medium,
                                                    height: 50,
                                                    width: 80,
                                                    fit: BoxFit.cover,
                                                    placeholder: (context,
                                                            url) =>
                                                        Container(
                                                            child: Center(
                                                                child: SizedBox(
                                                                    width: 20,
                                                                    height: 20,
                                                                    child:
                                                                        CircularProgressIndicator()))),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error),
                                                  )),
                                        title: Text(photoState
                                                .photos[index].photographer ??
                                            photoState
                                                .photos[index].photographer!),
                                      ),
                                    ),
                                  )
                                : Container(
                                    child: Center(
                                        child: Text('Load More...',
                                            style: TextStyle(
                                              color: Colors.red,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                            ))));
                          },
                          childCount: (photoState.hasReachedMax)
                              ? photoState.photos.length
                              : photoState.photos.length + 1,
                        ),
                      ),
                    ),
                  ],
                );
              },
            )
          : photoState is PhotoUninitialized
              ? Container(
                  child: Center(
                      child: SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator())))
              : SizedBox(),
    );
  }
}
