part of 'page.dart';

class PhotoGrid extends StatefulWidget {
  const PhotoGrid({Key? key}) : super(key: key);

  @override
  _PhotoGridState createState() => _PhotoGridState();
}

class _PhotoGridState extends State<PhotoGrid> {
  ScrollController controller = ScrollController();
  late PhotoBloc bloc;

  void onScroll() {
    double maxScroll = controller.position.maxScrollExtent;
    double currenSacroll = controller.position.pixels;

    if (currenSacroll == maxScroll) bloc.add(FetchPhotos(1));
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<PhotoBloc>(context);
    controller.addListener(onScroll);
    return BlocBuilder<PhotoBloc, PhotoState>(
      builder: (_, photoState) => photoState is PhotoLoaded
          ? OrientationBuilder(
              builder: (context, orientation) {
                return CustomScrollView(
                  controller: controller,
                  slivers: <Widget>[
                    SliverOverlapInjector(
                      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                          context),
                    ),
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount:
                              orientation == Orientation.portrait ? 2 : 3),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return (index < photoState.photos.length)
                              ? Card(
                                  child: InkWell(
                                    onTap: () {
                                      Get.to(PhotoDetail(
                                          photoState.photos[index]));
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          child: Hero(
                                              tag: photoState.photos[index].id,
                                              child: kIsWeb
                                                  ? Image.network(
                                                      photoState.photos[index]
                                                          .src.medium,
                                                      fit: BoxFit.cover,
                                                    )
                                                  : CachedNetworkImage(
                                                      imageUrl: photoState
                                                          .photos[index]
                                                          .src
                                                          .medium,
                                                      fit: BoxFit.cover,
                                                      placeholder: (context,
                                                              url) =>
                                                          Container(
                                                              child: Center(
                                                                  child: SizedBox(
                                                                      width: 30,
                                                                      height:
                                                                          30,
                                                                      child:
                                                                          CircularProgressIndicator()))),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Icon(Icons.error),
                                                    )),
                                        ),
                                        Text(
                                          photoState
                                                  .photos[index].photographer ??
                                              photoState
                                                  .photos[index].photographer!,
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              : Container(
                                  child: Center(
                                      child: Text('Load More...',
                                          style: TextStyle(
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ))));
                        },
                        childCount: (photoState.hasReachedMax)
                            ? photoState.photos.length
                            : photoState.photos.length + 1,
                      ),
                    ),
                  ],
                );
              },
            )
          : photoState is PhotoUninitialized
              ? Container(
                  child: Center(
                      child: SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator())))
              : SizedBox(),
    );
  }
}
